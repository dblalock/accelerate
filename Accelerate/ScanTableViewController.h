//
//  ScanTableViewController.h
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/14/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CBCentralManager.h>

@interface ScanTableViewController : UITableViewController <CBCentralManagerDelegate>

-(IBAction)clearPeripherals:(id)sender;

-(void) initWithPeripherals: (NSMutableArray*) discoveredPeripherals;

-(void) enterForeground;
-(void) enterBackground;

// Internal
- (void) clearPeriph;
- (void) clearPeriphForRow: (NSInteger)row;
- (void) scan: (bool)enable;
- (NSInteger)getRowForPeripheral: (CBPeripheral*)peripheral;

@end

