//
//  main.m
//  Accelerate
//
//  Created by DB on 5/4/14.
//  Copyright (c) 2014 IrrationalLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IRAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([IRAppDelegate class]));
	}
}
