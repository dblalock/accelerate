//
//  IRAppDelegate.h
//  Accelerate
//
//  Created by DB on 5/4/14.
//  Copyright (c) 2014 IrrationalLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
