//
//  DemoViewController.h
//  BLEDemo
//
//  Created by Tomas Henriksson on 1/17/12.
//  Copyright (c) 2012 connectBlue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CBPeripheral.h>

@interface DemoViewController : UIViewController <CBPeripheralDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UISegmentedControl *peripheralSegmentedControl;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *accRangeLabel;

@property (strong, nonatomic) IBOutlet UIProgressView *accXProgressView;
@property (strong, nonatomic) IBOutlet UIProgressView *accYProgressView;
@property (strong, nonatomic) IBOutlet UIProgressView *accZProgressView;

@property (strong, nonatomic) IBOutlet UILabel *tempLabel;
@property (strong, nonatomic) IBOutlet UILabel *batteryLabel;
@property (strong, nonatomic) IBOutlet UILabel *rssiLabel;

@property (strong, nonatomic) IBOutlet UISwitch *greenLedSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *redLedSwitch;

@property (strong, nonatomic) IBOutlet UISwitch *recordingSwitch;
@property (strong, nonatomic) IBOutlet UITextField *motionNameText;
@property (strong, nonatomic) IBOutlet UIStepper *motionNumberStepper;
@property (strong, nonatomic) IBOutlet UITextField *motionNumberText;

@property (strong, nonatomic) IBOutlet UILabel *spottedMotionLabel;

@property (nonatomic, readonly) BOOL recording;

- (IBAction)selectPeripheral:(id)sender;

- (IBAction)greenLedSwitchChanged:(id)sender;
- (IBAction)redLedSwitchChanged:(id)sender;

- (void) initWithPeripherals: (NSMutableArray*) discoveredPeripherals;

-(IBAction)toggleRecording:(id)sender;
-(IBAction)motionNumberChanged:(id)sender;

//-(void) writeAccelDataIfReady;

@end
