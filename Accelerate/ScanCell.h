//
//  ScanCell.h
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/19/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel* labelName;
@property (nonatomic, strong) IBOutlet UILabel* labelInfo;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* activityView;

@end
