//
//  DemoViewController.m
//  BLEDemo
//
//  Created by Tomas Henriksson on 1/17/12.
//  Copyright (c) 2012 connectBlue. All rights reserved.
//

#import "DemoViewController.h"
#import "DiscoveredPeripheral.h"
#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBUUID.h>
#import <CoreBluetooth/CBService.h>
#import <CoreBluetooth/CBCharacteristic.h>
#import <Foundation/NSException.h>
#import "BLEDefinitions.h"
#import "FileUtils.h"

//static const NSString* kACCEL_FILE_NAME = @"accel_test.csv";
static NSString* kDEFAULT_MOTION_NAME = @"TestMotion";
static const int X_INDEX = 0;
static const int Y_INDEX = 1;
static const int Z_INDEX = 2;


typedef enum
{
    DEMO_S_NOT_LOADED,
    DEMO_S_DISAPPEARED,
    
    DEMO_S_APPEARED_WAIT_SERVICE_SEARCH,
    DEMO_S_APPEARED_WAIT_CHARACT_SEARCH,
    DEMO_S_APPEARED_IDLE,
    
    DEMO_S_APPEARED_NO_CONNECT_PERIPH
    
} DEMO_State;

@implementation DemoViewController
{
    DEMO_State          state;
    
    NSTimer             *timer;
    
    BOOL                redLedWriting;
    BOOL                greenLedWriting;
    
    NSMutableArray      *discoveredPeripherals;
    NSMutableArray      *connectedPeripherals;
    CBPeripheral        *connectedPeripheral;
    
    CBService           *accService;
    CBService           *tempService;
    CBService           *batteryService;
    CBService           *ledService;
    
    CBCharacteristic    *accRangeCharact;
    CBCharacteristic    *accXCharact;
    CBCharacteristic    *accYCharact;
    CBCharacteristic    *accZCharact;
    CBCharacteristic    *tempCharact;
    CBCharacteristic    *batteryCharact;
    CBCharacteristic    *greenLedCharact;
    CBCharacteristic    *redLedCharact;
    
    NSInteger   range;
	
	int8_t currentAccelData[3];
}

@synthesize peripheralSegmentedControl;
@synthesize activityIndicator;
@synthesize accRangeLabel;
@synthesize accXProgressView;
@synthesize accYProgressView;
@synthesize accZProgressView;
@synthesize tempLabel;
@synthesize batteryLabel;
@synthesize rssiLabel;
@synthesize greenLedSwitch;
@synthesize redLedSwitch;

@synthesize motionNameText;
@synthesize motionNumberText;
@synthesize motionNumberStepper;
@synthesize recordingSwitch;

@synthesize spottedMotionLabel;

@synthesize recording;
//@synthesize currentAccelData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		for (int i = 0; i < 3; i++) {
			currentAccelData[i] = 0;
		}
    }
    return self;
}

- (void) initWithPeripherals: (NSMutableArray*) dp
{
    discoveredPeripherals = dp;
    
    connectedPeripherals = [[NSMutableArray alloc] init];
    
    connectedPeripheral = nil;
    
    accService = nil;
    tempService = nil;
    batteryService = nil;
    ledService = nil;
    
    accXCharact = nil;
    accYCharact = nil;
    accZCharact = nil;
    tempCharact = nil;
    batteryCharact = nil;
    redLedCharact = nil;
    greenLedCharact = nil;
    
    redLedWriting = FALSE;
    greenLedWriting = FALSE;
    
    state = DEMO_S_NOT_LOADED;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//================================================================
#pragma mark - View lifecycle
//================================================================

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    state = DEMO_S_DISAPPEARED;
	
	motionNameText.delegate = self; //new
	[motionNameText setText:kDEFAULT_MOTION_NAME];
}

- (void)viewDidUnload
{
    [self setAccXProgressView:nil];
    [self setAccYProgressView:nil];
    [self setAccZProgressView:nil];
    [self setTempLabel:nil];
    [self setGreenLedSwitch:nil];
    [self setRedLedSwitch:nil];
    [self setAccRangeLabel:nil];
    [self setBatteryLabel:nil];
    [self setActivityIndicator:nil];
    [self setPeripheralSegmentedControl:nil];
    [self setRssiLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    connectedPeripheral = nil;
    
    connectedPeripherals = nil;
    
    state = DEMO_S_NOT_LOADED;
	
}

- (BOOL)displayPeripheral: (CBPeripheral*)periph andCharacteristic: (CBCharacteristic*) charact
{
    char *p = (char*)charact.value.bytes;
    NSInteger val;
    BOOL ok = FALSE;
    
    if((periph == connectedPeripheral) && (p != nil))
    {
        if(charact == accRangeCharact)
        {
            range = (p[1] << 8) | p[0];
            accRangeLabel.text = [[NSString alloc] initWithFormat:@"Accel Range +-%dG", range];
            
            ok = TRUE;
        }
        else if(charact == accXCharact)
        {
            val = p[0] + 128;
            accXProgressView.progress = ((float)val) / 256.0;
			currentAccelData[X_INDEX] = p[0];
            
            ok = TRUE;
        }
        else if(charact == accYCharact)
        {
            val = p[0] + 128;
            accYProgressView.progress = ((float)val) / 256.0;
			currentAccelData[Y_INDEX] = p[0];
            
            ok = TRUE;
        }
        else if(charact == accZCharact)
        {
            val = p[0] + 128;
            accZProgressView.progress = ((float)val) / 256.0;
			currentAccelData[Z_INDEX] = p[0];
            
            ok = TRUE;
        }
        else if(charact == tempCharact)
        {
			// hack to make it report when the board thinks it sees something
			// (sent via misuse of the temperature service)
			if (p[0] > 10) {	//TODO make > like it should be after test
				tempLabel.text = [[NSString alloc] initWithFormat:@"Temp %d °C", p[0]];
			} else {
				[self showMessage:[self nameForMotionWithNumber:p[0]]];
			}
            
            ok = TRUE;
        }
        else if(charact == batteryCharact)
        {
            batteryLabel.text = [[NSString alloc] initWithFormat:@"Battery %d%%", p[0]];
            
            ok = TRUE;
        }
        else if((greenLedWriting == FALSE) && (charact == greenLedCharact))
        {
            greenLedSwitch.enabled = TRUE;
            
            if(p[0] != 0)
            {
                greenLedSwitch.on = TRUE;
            }
            else
            {
                greenLedSwitch.on = FALSE;
            }
            
            [activityIndicator stopAnimating];
            
            ok = TRUE;
        }
        else if((redLedWriting == FALSE) && (charact == redLedCharact))
        {
            redLedSwitch.enabled = TRUE;
            
            if(p[0] != 0)
            {
                redLedSwitch.on = TRUE;
            }
            else
            {
                redLedSwitch.on = FALSE;
            }
            
            //[activityIndicator stopAnimating];
            
            ok = TRUE;
        }
    }
	
	[self writeAccelDataIfReady];
    
    return ok;
}


- (BOOL) initCharacteristicForService: (CBService*)serv andCharact: (CBCharacteristic*)charact
{
    BOOL done = FALSE;
    //BOOL updated;
    
    if((connectedPeripheral != nil) && (connectedPeripheral.state == CBPeripheralStateConnected))
    {
        if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
           (memcmp(serv.UUID.data.bytes, accServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            accService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, accRangeCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accRangeCharact = charact;
				
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accRangeCharact];
                
                //if(updated == FALSE)
				[connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accXCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accXCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accXCharact];
                
                //if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
				
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
				
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accYCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accYCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accYCharact];
                
                //if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
				
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
				
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accZCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accZCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accZCharact];
                
                //if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
				
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
                
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, tempServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            tempService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, tempValueCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                tempCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: tempCharact];
				
                //if(updated == FALSE)
				[connectedPeripheral readValueForCharacteristic: charact];
                
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
				
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, batteryServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            batteryService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, batteryLevelCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                batteryCharact = charact;
                
                //[periph setNotifyValue: TRUE forCharacteristic:charact];
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: batteryCharact];
                
                //if(updated == FALSE)
				[connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, ledServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            ledService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, greenLedCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                greenLedCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: greenLedCharact];
                
                //if(updated == FALSE)
				[connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, redLedCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                redLedCharact = charact;
                
                //updated = [self displayPeripheral: connectedPeripheral andCharacteristic: redLedCharact];
                
                //if(updated == FALSE)
				[connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
        }
    }
    
    return done;
}


- (void) initConnectedPeripheral
{
    CBService*          service;
    CBCharacteristic*   charact;
    BOOL                ok;
    
    accRangeLabel.text = @"Accel Range Unavailable";
    accXProgressView.progress = 0;
    accYProgressView.progress = 0;
    accZProgressView.progress = 0;
    tempLabel.text = @"Temp Unavailable";
    rssiLabel.text = @"RSSI Unavailable";
    batteryLabel.text = @"Batt Level Unavailable";
    redLedSwitch.enabled = FALSE;
    greenLedSwitch.enabled = FALSE;
    
    accService = nil;
    tempService = nil;
    batteryService = nil;
    ledService = nil;
    
    accRangeCharact = nil;
    accXCharact = nil;
    accYCharact = nil;
    accZCharact = nil;
    tempCharact = nil;
    batteryCharact = nil;
    redLedCharact = nil;
    greenLedCharact = nil;
    
    connectedPeripheral.delegate = self;
    
    if(connectedPeripheral.services != nil)
    {
		for(int i = 0; i < connectedPeripheral.services.count; i++)
		{
			service = [connectedPeripheral.services objectAtIndex:i];
			
			if((service.characteristics != nil) && (service.characteristics.count > 0))
			{
				for(int j = 0; j < service.characteristics.count; j++)
				{
					charact = [service.characteristics objectAtIndex:j];
					
					ok = [self initCharacteristicForService:service andCharact:charact];
				}
			}
			else
			{
				ok = [self initCharacteristicForService:service andCharact:nil];
			}
		}
    }
	
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSData *data;
    CBUUID *uuid;
    
    if(ledService == nil)
    {
        data = [NSData dataWithBytes: ledServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if((redLedCharact == nil) || (greenLedCharact == nil))
    {
        [connectedPeripheral discoverCharacteristics:nil forService: ledService];
    }
	
    if(tempService == nil)
    {
        data = [NSData dataWithBytes: tempServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if(tempCharact == nil)
    {
        [connectedPeripheral discoverCharacteristics:nil forService: tempService];
    }
	
    if(batteryService == nil)
    {
        data = [NSData dataWithBytes: batteryServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if(batteryCharact == nil)
    {
        [connectedPeripheral discoverCharacteristics:nil forService: batteryService];
    }
	
    if(accService == nil)
    {
        data = [NSData dataWithBytes: accServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if((accRangeCharact == nil) || (accXCharact == nil) || (accYCharact == nil) || (accZCharact == nil))
    {
        [connectedPeripheral discoverCharacteristics:nil forService: accService];
    }
	
    if(arr.count > 0)
    {
        state = DEMO_S_APPEARED_WAIT_SERVICE_SEARCH;
        
        [connectedPeripheral discoverServices:arr];
    }
    else
    {
        state = DEMO_S_APPEARED_IDLE;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    DiscoveredPeripheral* dp;
    
    [super viewDidAppear:animated];
	
    connectedPeripheral = nil;
    [connectedPeripherals removeAllObjects];
    
//    NSLog(@"DemoViewController::viewDidAppear() - discoveredPeripherals count = %d",
//		  discoveredPeripherals.count);
	
	
	[self connectToSavedPeripherals];
	
	
    for(int i = 0; i < discoveredPeripherals.count; i++)
    {
        dp = [discoveredPeripherals objectAtIndex:i];
        
        if (dp.peripheral.isConnected == TRUE)
//		if (dp.state == CBPeripheralStateConnected || dp.state == CBPeripheralStateConnecting) //doesn't work
        {
            //if(connectedPeripheral == nil)
            //    connectedPeripheral = dp.peripheral;
            NSLog(@"DemoViewController::added a peripheral...");
            [connectedPeripherals addObject:dp.peripheral];
        }
    }
    
    [peripheralSegmentedControl removeAllSegments];
    
    if(connectedPeripherals.count > 0)
    {
        for(int i = 0; i < connectedPeripherals.count; i++)
        {
            [peripheralSegmentedControl insertSegmentWithTitle:[[connectedPeripherals objectAtIndex:i] name] atIndex:i animated:TRUE];
        }
        
        if((peripheralSegmentedControl.selected == FALSE) ||
           (peripheralSegmentedControl.selectedSegmentIndex >= connectedPeripherals.count))
        {
            peripheralSegmentedControl.selectedSegmentIndex = 0;
            connectedPeripheral = [connectedPeripherals objectAtIndex:0];
        }
        else
        {
            connectedPeripheral = [connectedPeripherals objectAtIndex:peripheralSegmentedControl.selectedSegmentIndex];
        }
        
        if(connectedPeripheral != nil)
        {
            //[connectedPeripheral readRSSI];
            
            self->timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target: self selector:@selector(timeout:) userInfo:nil repeats:TRUE];
        }
    }
    else
    {
        [peripheralSegmentedControl insertSegmentWithTitle:@"No Connected Peripheral" atIndex:0 animated:TRUE];
		
    }
    
    activityIndicator.hidesWhenStopped = TRUE;
    
    redLedWriting = FALSE;
    greenLedWriting = FALSE;
    
    if(connectedPeripheral != nil)
    {
        [activityIndicator startAnimating];
        
        [self initConnectedPeripheral];
    }
    else
    {
        [activityIndicator stopAnimating];
		
        accRangeLabel.text = @"Accelerometer Range Not Available";
        accXProgressView.progress = 0;
        accYProgressView.progress = 0;
        accZProgressView.progress = 0;
        tempLabel.text = @"Temperature Not Available";
        rssiLabel.text = @"RSSI Value Not Available";
        batteryLabel.text = @"Battery Level Not Available";
        redLedSwitch.enabled = FALSE;
        greenLedSwitch.enabled = FALSE;
        
        accService = nil;
        tempService = nil;
        batteryService = nil;
        ledService = nil;
        
        accRangeCharact = nil;
        accXCharact = nil;
        accYCharact = nil;
        accZCharact = nil;
        tempCharact = nil;
        batteryCharact = nil;
        redLedCharact = nil;
        greenLedCharact = nil;
        
        state = DEMO_S_APPEARED_NO_CONNECT_PERIPH;
    }
	
	//test if we're writing files successfully //TODO remove
//	NSLog(@"about to write file...");
//	NSString* fileName = [motionNameText text];
//	//	[FileUtils writeString:@"did it work?" toFile:@"test.txt"];
//	[FileUtils appendString:@"did it work?\n" toFile:fileName];
//	NSLog(@"in theory, just wrote file");
//	NSString* contents = [FileUtils readStringFromFile:fileName];
//	NSLog(@"read back contents: \"%@\" from file", contents);
}

- (void) viewWillDisappear:(BOOL)animated
{
    CBPeripheral* p;
    
    [super viewWillDisappear:animated];
	
    [activityIndicator stopAnimating];
    
    for(int i = 0; i < connectedPeripherals.count; i++) {
        p = [connectedPeripherals objectAtIndex:i];
		
        if(p.isConnected == TRUE)
//		if (p.state == CBPeripheralStateConnected)
        {
            if(accXCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accXCharact];
            
            if(accYCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accYCharact];
            
            if(accZCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accZCharact];
            
            if(tempCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:tempCharact];
            
            //if(batteryCharact != nil)
            //    [p setNotifyValue: FALSE forCharacteristic:batteryCharact];
        }
        
        p.delegate = nil;
    }
    
    if(self->timer != nil)
    {
        [self->timer invalidate];
        self->timer = nil;
    }
	
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
	
    state = DEMO_S_DISAPPEARED;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//================================================================
#pragma mark - Private
//================================================================

- (IBAction)selectPeripheral:(id)sender
{
    CBPeripheral *selectedPeripheral = nil;
    
    if(connectedPeripherals.count > 0)
    {
        if(peripheralSegmentedControl.selectedSegmentIndex < connectedPeripherals.count)
        {
            selectedPeripheral = [connectedPeripherals objectAtIndex:peripheralSegmentedControl.selectedSegmentIndex];
			
            if((selectedPeripheral != nil) && (selectedPeripheral != connectedPeripheral))
            {
                connectedPeripheral.delegate = nil;
				
                connectedPeripheral = selectedPeripheral;
				
                [activityIndicator startAnimating];
				
                [self initConnectedPeripheral];
            }
        }
    }
    else
    {
        peripheralSegmentedControl.selectedSegmentIndex = -1;
    }
}

- (IBAction)greenLedSwitchChanged:(id)sender
{
    NSData *data = nil;
    unsigned char buf[1] = {0};
    
    if(greenLedSwitch.isOn == TRUE)
        buf[0] = 1;
    
    data = [NSData dataWithBytes:buf length:1];
    
    greenLedWriting = TRUE;
	
    [connectedPeripheral writeValue:data forCharacteristic:greenLedCharact type:CBCharacteristicWriteWithResponse];
}

- (IBAction)redLedSwitchChanged:(id)sender
{
    NSData *data = nil;
    unsigned char buf[1] = {0};
    
    if(redLedSwitch.isOn == TRUE)
        buf[0] = 1;
    
    data = [NSData dataWithBytes:buf length:1];
    
    redLedWriting = TRUE;
    
    [connectedPeripheral writeValue:data forCharacteristic:redLedCharact type:CBCharacteristicWriteWithResponse];
}

//================================================================
#pragma mark - CBPeripheralDelegate
//================================================================

- (void)peripheral:(CBPeripheral *)periph didDiscoverServices:(NSError *)error
{
    CBService   *s;
    
    if(periph == connectedPeripheral)
    {
        for(int i = 0; i < periph.services.count; i++)
        {
            s = [[periph services] objectAtIndex:i];
			
            [periph discoverCharacteristics:nil forService: s];
			
            state = DEMO_S_APPEARED_WAIT_CHARACT_SEARCH;
        }
    }
}

- (void)peripheral:(CBPeripheral *)periph didDiscoverCharacteristicsForService:(CBService *)serv error:(NSError *)error
{
    CBCharacteristic* charact;
    BOOL ok;
	
    if(periph == connectedPeripheral)
    {
        for(int i = 0; i < serv.characteristics.count; i++)
        {
            charact = [serv.characteristics objectAtIndex:i];
			
            ok = [self initCharacteristicForService:serv andCharact:charact];
        }
    }
}

- (void)peripheral:(CBPeripheral *)periph didUpdateValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    if(error == nil)
    {
        [self displayPeripheral: periph andCharacteristic: charact];
    }
}

- (void)peripheral:(CBPeripheral *)periph didWriteValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    if(periph == connectedPeripheral)
    {
        if(charact == greenLedCharact)
            greenLedWriting = FALSE;
        else if(charact == redLedCharact)
            redLedWriting = FALSE;
        
        [periph readValueForCharacteristic: charact];
    }
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)periph error:(NSError *)error
{
    if((periph == connectedPeripheral) && (error == nil))
    {
        rssiLabel.text = [[NSString alloc] initWithFormat:@"RSSI %@ dB", periph.RSSI];
    }
}

- (void) timeout: (NSTimer*)tm
{
    if(self->timer == tm)
    {
        if((connectedPeripheral != nil) &&
		   (connectedPeripheral.state == CBPeripheralStateConnected))
        {
            [connectedPeripheral readRSSI];
        }
    }
}

//================================================================
#pragma mark - New Methods
//================================================================

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[motionNameText resignFirstResponder];
	return NO;
}

-(void) clearMotionNameText {
	[spottedMotionLabel setText:@""];
}

-(void) showMessage:(NSString*)msg {
	[spottedMotionLabel setText:msg];
	[self performSelector:@selector(clearMotionNameText) withObject:self afterDelay:3.0];
}

-(IBAction)toggleRecording:(id)sender {
	recording = recordingSwitch.isOn;
	
	if (recording) {
		NSLog(@"started recording motion: %@", [motionNameText text]);
	} else {
		NSLog(@"stopped recording motion: %@", [motionNameText text]);
		[self showMessage:[NSString stringWithFormat:@"Recorded a %@",
						   [motionNameText text]]];
//		[spottedMotionLabel setText:[NSString stringWithFormat:@"Recorded a %@",
//									 [motionNameText text]]];
//		[self performSelector:@selector(clearMotionNameText) withObject:self afterDelay:3.0];
	}
}

-(IBAction)motionNumberChanged:(id)sender {
	int num = round([motionNumberStepper value]);
	NSString* valueStr = [NSString stringWithFormat:@"%d", num];
	[motionNumberText setText:valueStr];
}

-(void) writeSample {
	// don't actually write anything unless we're recording
	if (! recording) return;
	
	NSString* fileName = [NSString stringWithFormat:@"%@%@.csv",
						  [motionNameText text],
						  [motionNumberText text]];
	NSString *line = [NSString stringWithFormat:@"%d, %d, %d,\n",
					  currentAccelData[X_INDEX],
					  currentAccelData[Y_INDEX],
					  currentAccelData[Z_INDEX]];
	[FileUtils appendString:line toFile:fileName];
}

-(void) writeAccelDataIfReady {
	//if all values nonzero, we've received all the data
	for (int i = 0; i < 3; i++) {
		if (currentAccelData[i] == 0)
			return;
	}
	
	[self writeSample];
	
	//set all values to zero so we don't write again until
	//we have a complete sample
	for (int i = 0; i < 3; i++) {
		currentAccelData[i]= 0;
	}
}

-(void) connectToSavedPeripherals {
	
}

-(NSString*) nameForMotionWithNumber:(char)num {
	switch(num) {
		case 1:
			return @"Down";
		case 2:
			return @"Up";
		case 3:
			return @"Left";
		case 4:
			return @"Right";
		default:
			return [NSString stringWithFormat:@"Motion %d", num];;
	}
}

@end
