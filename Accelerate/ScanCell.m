//
//  ScanCell.m
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/19/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import "ScanCell.h"

@implementation ScanCell

@synthesize labelName;
@synthesize labelInfo;
@synthesize activityView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
