//
//  DiscoveredPeripheral.m
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/15/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import "DiscoveredPeripheral.h"

#import <CoreBluetooth/CBPeripheral.h>

NSString * const kSAVED_PERIPHERALS_KEY = @"savedPeripheral";

@implementation DiscoveredPeripheral

//@synthesize peripheral;
@synthesize advertisment;
@synthesize rssi;
@synthesize state;

- (DiscoveredPeripheral*) initWithPeripheral: (CBPeripheral*) newPeripheral andAdvertisment: (NSDictionary*) newAdvertisment andRssi: (NSNumber*) newRssi
{
    self.peripheral = newPeripheral;
    self.advertisment = newAdvertisment;
    self.rssi = newRssi;
    self.state = DP_STATE_IDLE;
    
    return self;
}

+(void)savePeripheral:(CBPeripheral*)p {
	if (p==nil) return;
	if (p.name == nil) return;
	
	// store this device to pair with automatically in the future
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray* savedDevices = [defaults objectForKey:kSAVED_PERIPHERALS_KEY];
	if (savedDevices == nil) {
		savedDevices = [[NSMutableArray alloc] init];
	}
	if (! [savedDevices containsObject:p.name]) {
		[savedDevices addObject:p.name];
	}
	[defaults setObject:savedDevices forKey:kSAVED_PERIPHERALS_KEY];
}

+(void)unsavePeripheral:(CBPeripheral*)p {
	if (p==nil) return;
	if (p.name == nil) return;
	
	// stop storing this device to pair with automatically in the future
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray* savedDevices = [[defaults objectForKey:kSAVED_PERIPHERALS_KEY] mutableCopy];
	if (savedDevices == nil) {
		savedDevices = [[NSMutableArray alloc] init];
	} else {
		[savedDevices removeObject:p.name];
	}
	[defaults setObject:savedDevices forKey:kSAVED_PERIPHERALS_KEY];
}

+(BOOL)peripheralIsSaved:(CBPeripheral*)p {
	if (p==nil) return FALSE;
	if (p.name == nil) return FALSE;
	
	// store this device to pair with automatically in the future
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	NSMutableSet* savedDevices = [defaults objectForKey:kSAVED_PERIPHERALS_KEY];
	return (savedDevices != nil && [savedDevices containsObject:p.name]);
}

@end
